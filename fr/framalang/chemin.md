<h1>Le chemin d'une traduction</h1>
Participer à Framalang, ce n'est pas simplement traduire... En réalité, chaque membre du groupe peut :

<h2>Proposer une traduction</h2>

Ce travail de veille et de proposition d'article à traduire est très important, et nous sommes toujours avides de nouveaux contenus à traduire ! Nous commençons généralement le sujet de ces email par `[Proposition]`, avec dans l'email le lien vers la source, l'auteur et une rapide présentation du propos.

<h2>Accepter une proposition</h2>

Vous voyez un article sur lequel vous seriez prêt-e à travailler ? Dites-le en répondant à l'email de proposition. En général, à partir de 2, 3 réponses enthousiastes, on lance la traduction.

<h2>Contacter l'auteur-e d'un article proposé</h2>

Si la proposition de traduction n'est pas sous une licence permettant l'adaptation, il faut en contacter l'auteur·trice pour demander une autorisation de traduction.

<h2>Padifier l'article</h2>

Il s'agit simplement de copier l'article original sur un <a href="http://framapad.org">framapad</a> (outil d'écriture collaborative). Pensez à mentionner le titre, l'auteur, la licence et la source, et à sauter 2 lignes entre chaque paragraphe pour que la traduction se fasse aisément. Nous avons l'habitude d'utiliser un dossier [MyPads](https://mypads.framapad.org) partagé pour conserver nos traductions (voir [sur la page principale](index.html)).

Une fois le pad prêt, il suffit de l'envoyer au groupe dans un nouvel email marqué `[Traduction]` dans son objet.

<h2>Traduire l'article</h2>

C'est la partie où on retrouve le plus de monde, souvent les traductions vont vite ! Il suffit d'aller sur le pad et d'écrire (pensez à mettre votre pseudo et à choisir votre couleur dans l'icône en haut à droite). Toutes les traductions en cours ou passées sont disponibles dans notre outil de suivi des pads.

<h2>Proposer chapô et illustration</h2>

À l'intérieur du pad, vous pouvez rédiger le chapô : une introduction à cet article qui présente l'auteur-e, le propos, et pourquoi le groupe a trouvé important de le partager auprès du public francophone.

Vous pouvez aussi proposer des illustrations (Libres) à l'aide d'un lien et en mentionnant auteur, titre et licence de l'image (vous pouvez aussi partager ces images via notre outil <a href="https://framapic.org/">Framapic</a> !).

Pour trouver des images libres de droit, il y a [le méta-moteur de Creative Commons](http://search.creativecommons.org/) !

<h2>Relire la traduction</h2>

C'est un travail essentiel. N'hésitez pas à imposer vos choix : la traduction de l'autre n'est pas sacrée, et nous sommes dans une dynamique de confiance. On a souvent plus de recul à la relecture qu'à la traduction, donc une meilleure approche de la bonne tournure à choisir.

Ce n'est que lorsque vous n'êtes pas sûr-e de votre proposition que vous la signalez à côté `/*en la mettant dans ces signes*/` (attention : vous laisserez alors plus de travail aux personnes qui mettront l'article sur le blog !). Une bonne relecture est une relecture où tous les choix de traductions ont été tranchés ;).

<h2>Bloguifier la traduction</h2>

Certain-e-s membres de Framalang ont des accès au Framablog. Leur travail, [documenté ici](blog.html), consiste à prendre votre traduction, la vider de l'anglais, trancher dans les derniers choix (il faut qu'il y en ait le moins possible, siouplé), l'illustrer, lui écrire un chapô, la mettre en page...

Si vous vous sentez assez à l'aise dans le groupe (pour savoir motiver les troupes) et l'envie de faire ce travail-là, n'hésitez pas à le dire au groupe !
