# Summary

* [Accueil](README.md)


## Projets communautaires

* [Framablog - articles & ITW](framablog/README.md)
  * [Framablog - interviews](framablog/interviews.md)
  * [Framablog - articles](framablog/articles.md)
  * [Framablog - mise en page](framablog/mise-en-page.md)
* [Framabook - édition libre](framabook/README.md)
* [Framacode - développement](framacode/README.md)
  * [Contribuer facilement sur Framagit (traduction, rédaction)](framacode/git-en-ligne.md)
  * [Contribuer aux documentations](framacode/gitbook.md)
  * [Methode de travail git](framacode/workflow-git.md)
* [Framakey - clé usb](framakey/README.md)
* [Framalang - traduction](framalang/README.md)
  * [Framalang - Bienvenue](framalang/bienvenue.md)
  * [Framalang - Processus](framalang/chemin.md)
  * [Framalang - Outils](framalang/outils.md)
  * [Framalang - Vers le blog](framalang/blog.md)
* [Framalibre - l'annuaire](framalibre/README.md)

## Dégoogliser

* [Flyers, affiches, stickers](print/README.md)
* [Liens essentiels](essentiels/README.md)
* [Conférences, présentations](conferences/README.md)
